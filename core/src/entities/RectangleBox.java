package entities;

import com.badlogic.gdx.physics.box2d.World;

public class RectangleBox extends Box {


    public RectangleBox(World world, float posX, float posY, float rotation) {
        super(world, posX, posY, rotation);
        name = "rectangle_stone";
        createImage();
        createBody();
    }

    public RectangleBox(World world, float posX, float posY) {
        super(world, posX, posY, 0);
        name = "rectangle_stone";
        createImage();
    }

    public RectangleBoxMemento createMemento() {
        return new RectangleBoxMemento(this.posX, this.body.getPosition().y, rotation);
    }

    public void removeBody() {
        body.setActive(false);
    }

    public class RectangleBoxMemento {
        private float xPosition;
        private float yPosition;
        private float rot;

        RectangleBoxMemento(float x, float y, float rotation) {
            this.xPosition = x;
            this.yPosition = y;
            this.rot = rotation;

        }

        public float getXPosition() {
            return xPosition;
        }

        public float getYPosition() {
            return yPosition;
        }

        public float getRot() {
            return rot;
        }
    }

}
