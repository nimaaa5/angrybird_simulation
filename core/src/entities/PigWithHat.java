package entities;


public class PigWithHat extends PigDecorator {
    public PigWithHat(BasicPig pig) {
        super(pig);
        this.name = "hat";
    }

    public BasicPig getPig(){
        return this.basicPig;
    }

    @Override
    public void assemble() {
        super.assemble();
        createAccessoryImage();
        createAccessoryBody();
        weldAccessoryToBody();
    }




}
