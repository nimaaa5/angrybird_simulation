package entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;

public class Container extends Box {

    private ArrayList<Box> boxes;
    private Group group;
    private int containerNumber;
    private int rectangleNumber;
    private int squareNumber;

    public Container(World world, float posX, float posY,
                     int containerNumber, int rectangleNumber, int squareNumber) {
        super(world, posX, posY, 0);
        this.containerNumber = containerNumber;
        this.rectangleNumber = rectangleNumber;
        this.squareNumber = squareNumber;
        boxes = new ArrayList<Box>();
        group = new Group();
        this.createImage();
        this.createBody();
    }


    @Override
    public void draw(SpriteBatch batch) {
        batch.begin();
        group.setPosition(body.getPosition().x, body.getPosition().y);
        float degree = (float) Math.toDegrees(body.getAngle());
        group.setRotation(degree);
        group.draw(batch, 1);
        batch.end();
    }

    @Override
    public void createImage() {
        createSquareImage();
        createRectangleImage();
        group.setPosition(posX, posY);
        group.setWidth(containerNumber * boxes.get(0).image.getWidth());
        group.setHeight(containerNumber * boxes.get(0).image.getWidth());
    }


    @Override
    public void createBody() {
        BodyDef bd = new BodyDef();
        bd.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bd);
        for (int i = 0; i < containerNumber; i++) {
            FixtureDef fd = createFixtureDef(i);
            body.createFixture(fd);
        }
        body.setTransform(group.getX(), group.getY(), group.getRotation());
    }


    private FixtureDef createFixtureDef(int columnNumber) {
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(setVertices(columnNumber));
        FixtureDef fd = new FixtureDef();
        fd.density = 10;
        fd.friction = 0.5f;
        fd.restitution = 0.3f;
        fd.shape = polygonShape;
        return fd;
    }


    private Vector2[] setVertices(int columnNumber) {
        int NUMBER_OF_SQUARE_VERTICES = 4;
        Vector2[] vertices = new Vector2[NUMBER_OF_SQUARE_VERTICES];
        int lastImageIndex = getLastImageIndex(columnNumber);
        vertices[0] = new Vector2(boxes.get(columnNumber).posX, boxes.get(columnNumber).posY);
        vertices[1] = new Vector2(boxes.get(columnNumber).posX + boxes.get(columnNumber).image.getWidth(),
                boxes.get(columnNumber).posY);
        vertices[2] = new Vector2(
                boxes.get(lastImageIndex).posX,
                boxes.get(lastImageIndex).posY + boxes.get(lastImageIndex).image.getHeight());
        vertices[3] = new Vector2(
                boxes.get(lastImageIndex).posX + boxes.get(lastImageIndex).image.getWidth(),
                boxes.get(lastImageIndex).posY + boxes.get(lastImageIndex).image.getHeight());

        return vertices;

    }

    private int getLastImageIndex(int columnNumber) {
        int lastImageIndex = 0;
        for (int i = columnNumber; i < boxes.size(); i = i + containerNumber) {
            lastImageIndex = i;
        }
        return lastImageIndex;
    }

    private void addBox(Box box) {
        boxes.add(box);
    }

    private void createSquareImage() {
        Box box;
        box = new SquareBox(world, group.getX(), group.getY());
        addBox(box);
        group.addActor(((SquareBox) box).image);
        for (int i = 1; i < squareNumber; i++) {
            if (i % containerNumber == 0) {
                box = new SquareBox(world, group.getX() + (i % containerNumber) * ((SquareBox) box).image.getWidth(),
                        group.getY() + (i / containerNumber) * ((SquareBox) box).image.getHeight());
            } else {
                box = new SquareBox(world, group.getX() + (i % containerNumber) * ((SquareBox) box).image.getWidth(),
                        group.getY() + (i / containerNumber) * ((SquareBox) box).image.getHeight());
            }
            addBox(box);
            group.addActor(((SquareBox) box).image);
        }
    }

    private void createRectangleImage() {
        int currentBox = boxes.size();
        for (int i = 0; i < rectangleNumber; i++) {
            RectangleBox box = new RectangleBox(world, getImageXPosition(currentBox + i), getImageYPosition(currentBox + i));
            boxes.add(box);
            group.addActor(box.image);
        }
    }

    private float getImageXPosition(int currentBox) {
        int lowerBox = currentBox - containerNumber;
        return boxes.get(lowerBox).posX;
    }

    private float getImageYPosition(int currentBox) {
        int lowerBox = currentBox - containerNumber;
        return boxes.get(lowerBox).posY + boxes.get(lowerBox).image.getHeight();
    }

    @Override
    public boolean isInside(float x, float y) {
        System.out.println(x + "    " + y);
        System.out.println(group.getX() + "     " + group.getY());
        System.out.println(group.getHeight() + "     " + group.getHeight());
        System.out.println(group.getX() + group.getWidth() + "      " + (float) (group.getY() + group.getHeight()));
        System.out.println("----------------------------------------");
        return (x < (group.getX() + group.getWidth() + 2) && x > group.getX() - 2)
                && (y < (float) (group.getY() + group.getHeight() + 2) && y > group.getY() - 2);
    }


    public ContainerMemento createMemento() {
        return new ContainerMemento(posX, posY, containerNumber, rectangleNumber, squareNumber);
    }

    public void removeContainer() {
        body.setActive(false);
    }

    public class ContainerMemento {
        private float xPos;
        private float yPos;
        private int numOfContainer;
        private int numOfRectangle;
        private int numOfSquare;

        public ContainerMemento(float x, float y, int cont, int rect, int square) {
            this.xPos = x;
            this.yPos = y;
            this.numOfContainer = cont;
            this.numOfRectangle = rect;
            this.numOfSquare = square;
        }

        public float getxPos() {
            return xPos;
        }

        public float getyPos() {
            return yPos;
        }

        public int getNumOfContainer() {
            return numOfContainer;
        }

        public int getNumOfSquare() {
            return numOfSquare;
        }

        public int getNumOfRectangle() {
            return numOfRectangle;
        }
    }
}
