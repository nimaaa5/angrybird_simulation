package entities;

import com.badlogic.gdx.physics.box2d.World;

public class BasicPig extends Pig {

    public BasicPig(World world, float posX, float posY, float rotation) {
        super(world, posX, posY, rotation);
        this.name = "healthy_pig";
    }

    @Override
    public void assemble() {
        createImage();
        createBody();
    }



    public PigMemento createMemento() {
        return new PigMemento(posX, posY, rotation);
    }

    public void removeBody(){
        body.setActive(false);
    }

    public class PigMemento {
        private float xPosition;
        private float yPosition;
        private float rot;

        public PigMemento(float x, float y, float rot) {
            this.xPosition = x;
            this.yPosition = y;
            this.rot = rot;
        }

        public float getxPosition() {
            return xPosition;
        }

        public float getyPosition() {
            return yPosition;
        }

        public float getRot() {
            return rot;
        }
    }
}
