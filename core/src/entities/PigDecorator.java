package entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.game.Constant;

public abstract class PigDecorator extends Pig {

    protected BasicPig basicPig;


    public PigDecorator(BasicPig pig) {
        super(pig.world, pig.posX, pig.posY, pig.rotation);
        this.basicPig = pig;
    }

    @Override
    public void assemble() {
        this.basicPig.assemble();
    }

    protected void createAccessoryImage() {
        Sprite imageAccesory = Constant.TEXTURE_ATLAS.createSprite(name);
        this.image = new Image(imageAccesory);
        this.image.setSize(imageAccesory.getWidth() * Constant.SCALE,
                imageAccesory.getHeight() * Constant.SCALE);
        this.image.setPosition(posX, posY + basicPig.image.getHeight());
    }

    void createAccessoryBody() {
        body = PHYSICS_SHAPE_CACHE.createBody(name, world, Constant.SCALE, Constant.SCALE);
        body.setTransform(posX, posY, rotation);
    }

    void weldAccessoryToBody() {
        WeldJointDef weldJointDef = new WeldJointDef();
        weldJointDef.bodyA = basicPig.body;
        weldJointDef.bodyB = body;
        weldJointDef.localAnchorA.set(new Vector2(-.5f, 1.8f));
        world.createJoint(weldJointDef);
    }

    public void removeBody(){
        this.body.setActive(false);
    }

    public void draw(SpriteBatch batch) {
        batch.begin();
        this.basicPig.image.setPosition(this.basicPig.body.getPosition().x,
                this.basicPig.body.getPosition().y);
        basicPig.rotation = (float) Math.toDegrees(basicPig.body.getAngle());
        this.basicPig.image.setRotation(basicPig.rotation);
        this.basicPig.image.draw(batch, 1);
        this.image.setPosition(body.getPosition().x, body.getPosition().y);
        rotation = (float) Math.toDegrees(body.getAngle());
        this.image.setRotation(rotation);
        this.image.draw(batch, 1);
        batch.end();
    }
}
