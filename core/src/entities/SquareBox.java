package entities;

import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.Constant;

public class SquareBox extends Box {

    public SquareBox(World world, float posX, float posY, float rotation) {
        super(world, posX, posY, rotation);
        name = "square_stone";
        createImage();
        createBody();
    }


    SquareBox(World world, float posX, float posY) {
        super(world, posX, posY, 0);
        name = "square_stone";
        createImage();
    }

    public SquareBoxMemento createMemento() {
        return new SquareBoxMemento(this.posX, this.body.getPosition().y, rotation);
    }

    public void removeBody() {
        body.setActive(false);
    }

    public void setMemento(SquareBoxMemento memento) {
        posX = memento.getXPosition();
        posY = memento.getYPosition();
        rotation = memento.getRot();
    }


    public class SquareBoxMemento {

        private float xPosition;
        private float yPosition;
        private float rot;

        public SquareBoxMemento(float x, float y, float rotation) {
            this.xPosition = x;
            this.yPosition = y;
            this.rot = rotation;

        }

        public float getXPosition() {
            return xPosition;
        }

        public float getYPosition() {
            return yPosition;
        }

        public float getRot() {
            return rot;
        }
    }


}
