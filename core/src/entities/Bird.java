package entities;


import Strategy.MotionStrategy;
import Strategy.ProjectileMotion;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.codeandweb.physicseditor.PhysicsShapeCache;
import com.mygdx.game.Constant;
import theme.Ground;

public class Bird {

    private Image birdImage;
    private MotionStrategy motionStrategy;
    private Vector2 initialPoint;
    private Vector2 initialVelocity;
    private Body body;
    private boolean isReleased;
    private boolean isCharging;
    private static final PhysicsShapeCache PHYSICS_SHAPE_CACHE = new PhysicsShapeCache("physics.xml");

    public Bird() {
        this.initialPoint = new Vector2();
        this.initialVelocity = new Vector2();
        this.body = PHYSICS_SHAPE_CACHE.createBody("healthy_red_bird",
                Constant.getWorld(), Constant.SCALE, Constant.SCALE);
        this.body.setActive(false);
        this.body.setTransform(10, 10, 0);
        this.birdImage = new Image(Constant.TEXTURE_ATLAS.createSprite("healthy_red_bird"));
        this.birdImage.setSize(birdImage.getWidth() * Constant.SCALE,
                birdImage.getHeight() * Constant.SCALE);
    }

    public void setMotionStrategy(MotionStrategy motionStrategy) {
        this.motionStrategy = motionStrategy;
    }

    public void draw() {
        if (isCharging && isReleased) {
            Constant.getBatch().begin();
            moveBody();
            birdImage.setPosition(body.getPosition().x, body.getPosition().y);
            float degree = (float) Math.toDegrees(body.getAngle());
            birdImage.setRotation(degree);
            birdImage.draw(Constant.getBatch(), 1);
            Constant.getBatch().end();
        } else {
            Constant.getBatch().begin();
            birdImage.setPosition(body.getPosition().x, body.getPosition().y);
            birdImage.draw(Constant.getBatch(), 1);
            Constant.getBatch().end();
        }
    }

    private float time = 0f;
    private boolean flag = false;

    private void moveBody() {
        body.setActive(true);
        Constant.getWorld().setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        flag = true;
                        body.setAwake(true);
                    }
                });
            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }

        });
        if (!flag) {
            body.setTransform(motionStrategy.getX(time),
                    motionStrategy.getY(time), 0);
            time += .04;
        } else if (body.getPosition().y == 4.5) {
            body.setActive(false);

        }

    }


    public void setInitialVelocity() {
        initialVelocity.x = (10 - initialPoint.x) * 5;
        initialVelocity.y = (10 - initialPoint.y) * 5;
        motionStrategy = new ProjectileMotion(initialVelocity, initialPoint);
    }

    public void setPosition() {
        initialPoint.x = body.getPosition().x;
        initialPoint.y = body.getPosition().y;
    }


    public Image getBirdImage() {
        return birdImage;
    }

    public void setReleased(boolean released) {
        isReleased = released;
    }

    public void setCharging(boolean charging) {
        isCharging = charging;
    }

    public Body getBody() {
        return body;
    }
}
