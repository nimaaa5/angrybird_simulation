package entities;

import com.badlogic.gdx.physics.box2d.World;

public abstract class Pig extends BaseEntity{
    public Pig(World world, float posX, float posY, float rotation) {
        super(world, posX, posY, rotation);
    }

    public abstract void assemble();

}
