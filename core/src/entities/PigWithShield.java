package entities;

public class PigWithShield extends PigDecorator {

    public PigWithShield(BasicPig pig) {
        super(pig);
        this.name = "shield";
    }
    @Override
    public void assemble() {
        super.assemble();
        createAccessoryImage();
        createAccessoryBody();
        weldAccessoryToBody();
    }

    public BasicPig getPig(){
        return this.basicPig;
    }
}
