package entities;

import com.badlogic.gdx.physics.box2d.World;

public abstract class Box extends BaseEntity {
    public Box(World world, float posX, float posY, float rotation) {
        super(world, posX, posY, rotation);
    }

}
