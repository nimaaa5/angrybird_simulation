package entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.codeandweb.physicseditor.PhysicsShapeCache;
import com.mygdx.game.Constant;

public abstract class
BaseEntity {

    protected World world;
    protected String name;
    protected Body body;
    protected float posX;
    protected float posY;
    protected float rotation;
    protected Image image;
    protected static final PhysicsShapeCache PHYSICS_SHAPE_CACHE = new PhysicsShapeCache("physics.xml");

    public BaseEntity(World world, float posX, float posY, float rotation) {
        this.world = world;
        this.posX = posX;
        this.posY = posY;
        this.rotation = rotation;
    }

    public void draw(SpriteBatch batch) {
        batch.begin();
        this.image.setPosition(body.getPosition().x, body.getPosition().y);
        rotation = (float) Math.toDegrees(body.getAngle());
        this.image.setRotation(rotation);
        this.image.draw(batch, 1);
        batch.end();
    }

    public void createImage() {
        Sprite sprite = Constant.TEXTURE_ATLAS.createSprite(name);
        this.image = new Image(sprite);
        this.image.setSize(sprite.getWidth() * Constant.SCALE,
                sprite.getHeight() * Constant.SCALE);
        this.image.setPosition(posX, posY);
    }

    public void createBody() {
        body = PHYSICS_SHAPE_CACHE.createBody(name, world, Constant.SCALE, Constant.SCALE);
        body.setTransform(posX, posY, rotation);
    }

    public boolean isInside(float x, float y) {
        return (x < (image.getX() + image.getWidth() + 2) && x > image.getX() - 2)
                && (y < (float) (image.getY() + image.getHeight() + 2) && y > image.getY() - 2);
    }

    public Body getBody() {
        return body;
    }
}
