package theme;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Constant;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class MorningSlingshot extends Slingshot {

    public MorningSlingshot() {
        sprite = new Image(Constant.TEXTURE_ATLAS.createSprite("slingshot"));
        part_sprite = new Image(Constant.TEXTURE_ATLAS.createSprite("slingpart"));
        sprite.setSize(sprite.getWidth() * Constant.SCALE,
                sprite.getHeight() * Constant.SCALE);
        part_sprite.setSize(part_sprite.getWidth() * Constant.SCALE,
                part_sprite.getHeight() * Constant.SCALE);
    }

    @Override
    public void setPosition(Ground ground) {
        sprite.setRotation(0);
        sprite.setPosition(10, ground.sprite.getHeight() * Constant.SCALE + 4);
        sprite.setZIndex(0);
        part_sprite.setPosition(10f,
                ground.sprite.getHeight() * Constant.SCALE + 6.5f);
        part_sprite.setRotation(0);
        part_sprite.setZIndex(2);
    }

    @Override
    public void draw(SpriteBatch batch) {

    }
}
