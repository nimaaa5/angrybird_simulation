package theme;

import com.mygdx.game.Constant;

public class AfternoonGround extends Ground{

    public AfternoonGround() {
        sprite = Constant.TEXTURE_ATLAS.createSprite("afternoon_ground");
        sprite.setSize(Constant.VP_WIDTH,
                sprite.getHeight() * Constant.SCALE);
    }
}
