package theme;

import com.mygdx.game.Constant;

public class MorningBackground extends Background {
    MorningBackground() {
        sprite = Constant.TEXTURE_ATLAS.createSprite("background");
        sprite.setSize(Constant.VP_WIDTH,
                Constant.VP_HEIGHT);
    }


    @Override
    protected void setPosition(Ground ground) {
        sprite.setPosition(0, ground.sprite.getHeight() - 3);
    }
}
