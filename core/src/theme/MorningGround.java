package theme;

import com.mygdx.game.Constant;

public class MorningGround extends Ground {

    public MorningGround(){
        sprite = Constant.TEXTURE_ATLAS.createSprite("ground");
        sprite.setSize(Constant.VP_WIDTH ,
                sprite.getHeight() * Constant.SCALE);
    }
}
