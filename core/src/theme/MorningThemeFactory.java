package theme;

public class MorningThemeFactory extends ThemeFactory {

    private MorningThemeFactory() {
    }

    public static ThemeFactory getInstance() {
        if (instance == null) {
            instance = new MorningThemeFactory();
        }
        return instance;
    }

    @Override
    public Background createBackground() {
        return new MorningBackground();
    }

    @Override
    public Ground createGround() {
        return new MorningGround();
    }

    @Override
    public Slingshot createSlingshot() {
        return new MorningSlingshot();
    }


}
