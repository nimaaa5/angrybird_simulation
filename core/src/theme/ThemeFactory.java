package theme;

public abstract class ThemeFactory {
    protected static ThemeFactory instance = null;

    protected ThemeFactory() {

    }

    public static ThemeFactory getInstance() {
        return instance;
    }

    public static void setInstance() {
        ThemeFactory.instance = null;
    }

    public abstract Background createBackground();

    public abstract Ground createGround();

    public abstract Slingshot createSlingshot();
}
