package theme;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public abstract class Slingshot {
    protected Image part_sprite;
    protected Image sprite;
    protected ShapeRenderer shapeRenderer;
    protected boolean isCharging = false;
    protected boolean isReleased = false;

    public Slingshot() {
        shapeRenderer = new ShapeRenderer();
    }

    public abstract void setPosition(Ground ground);

    public abstract void draw(SpriteBatch batch);

    void drawShot() {
        if (isCharging && !isReleased) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(0.2f, 0f, 0f, 1f);
            shapeRenderer.triangle(196f, 155f, 196f, 165f, Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY());
            shapeRenderer.triangle(176f, 155f, 175f, 170f, Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY());
            shapeRenderer.end();
        } else {
            isReleased = false;
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(0.2f, 0f, 0f, 1f);
            shapeRenderer.triangle(196f, 155f, 196f, 165f, 186f, 158f);
            shapeRenderer.triangle(176f, 155f, 175f, 170f, 186f, 158f);
            shapeRenderer.end();
        }
    }

    public void setCharging(boolean charging) {
        isCharging = charging;
    }

    public void setReleased(boolean released) {
        isReleased = released;
    }
}
