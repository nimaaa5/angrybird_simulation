package theme;

import com.mygdx.game.Constant;

public class AfternoonBackground extends Background {

     AfternoonBackground() {
        sprite = Constant.TEXTURE_ATLAS.createSprite("afternoon_background");
        sprite.setSize(Constant.VP_WIDTH,
                Constant.VP_HEIGHT);
    }

    @Override
    protected void setPosition(Ground ground) {
        sprite.setPosition(0, ground.sprite.getHeight() - 3);
    }
}
