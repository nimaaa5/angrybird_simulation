package theme;

import com.badlogic.gdx.graphics.g2d.Sprite;

public abstract class Background {
    protected Sprite sprite;

    protected abstract void setPosition(Ground ground);
}
