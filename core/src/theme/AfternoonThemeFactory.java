package theme;

public class AfternoonThemeFactory extends ThemeFactory {

    private AfternoonThemeFactory() {
    }

    public static ThemeFactory getInstance() {
        if (instance == null) {
            instance = new AfternoonThemeFactory();
        }
        return instance;
    }

    @Override
    public Background createBackground() {
        return new AfternoonBackground();
    }

    @Override
    public Ground createGround() {
        return new AfternoonGround();
    }

    @Override
    public Slingshot createSlingshot() {
        return new AfternoonSlingshot();
    }
}
