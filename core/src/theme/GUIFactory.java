package theme;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GUIFactory {
    private Ground ground;
    private Background background;
    private Slingshot slingshot;

    public void buildGUI(ThemeFactory themeFactory) {
        ground = themeFactory.createGround();
        ground.setPosition();

        background = themeFactory.createBackground();
        background.setPosition(ground);
        slingshot = themeFactory.createSlingshot();
        slingshot.setPosition(ground);
    }

    public void drawGUI(SpriteBatch batch) {
        batch.begin();
        background.sprite.draw(batch);
        ground.sprite.draw(batch);
        slingshot.sprite.draw(batch, 1);
        batch.end();
    }

    public void drawSlingShotPart(SpriteBatch batch) {
        batch.begin();
        slingshot.part_sprite.draw(batch, 1);
        batch.end();
    }

    public void drawShot() {
        slingshot.drawShot();
    }

    public Slingshot getSlingshot() {
        return slingshot;
    }

    public Ground getGround() {
        return ground;
    }
}
