package theme;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Constant;
import com.badlogic.gdx.scenes.scene2d.ui.Image;


public class AfternoonSlingshot extends Slingshot {

    public AfternoonSlingshot() {
        sprite = new Image(Constant.TEXTURE_ATLAS.createSprite("slingshot_"));
        part_sprite = new Image(Constant.TEXTURE_ATLAS.createSprite("part_slingshot"));
        sprite.setSize(sprite.getWidth() * Constant.SCALE,
                sprite.getHeight() * Constant.SCALE);
        part_sprite.setSize(part_sprite.getWidth() * Constant.SCALE,
                part_sprite.getHeight() * Constant.SCALE);
    }

    @Override
    public void setPosition(Ground ground) {
        sprite.setRotation(0);
        sprite.setPosition(11, ground.sprite.getHeight() * Constant.SCALE + 4);
        part_sprite.setPosition(10f,
                ground.sprite.getHeight() * Constant.SCALE + 7.2f);
        part_sprite.setRotation(0);
        part_sprite.setZIndex(1);
    }

    @Override
    public void draw(SpriteBatch batch) {
    }
}
