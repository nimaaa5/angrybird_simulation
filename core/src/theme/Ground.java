package theme;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.mygdx.game.Constant;

public abstract class Ground {
    protected Sprite sprite;
    protected Body body;

    protected void setPosition() {
        sprite.setPosition(0, 0);
    }

    public void createBody(){
        if (body != null) Constant.getWorld().destroyBody(body);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;

        FixtureDef fixtureDef = new FixtureDef();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constant.getCamera().viewportWidth, 4.5f);

        fixtureDef.shape = shape;
        fixtureDef.friction = 0;
        fixtureDef.density = 2;

        body = Constant.getWorld().createBody(bodyDef);
        body.createFixture(fixtureDef);
        body.setTransform(0, 0, 0);

        shape.dispose();
    }
}
