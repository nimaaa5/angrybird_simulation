package command;

public interface AddCommand {
     void execute();
     void undo();
     void redo();
}
