package command;

import java.util.Stack;

public class AddCommandManager {

    private Stack<AddCommand> undoStack = new Stack<AddCommand>();
    private Stack<AddCommand> redoStack = new Stack<AddCommand>();

    public void executeCommand(AddCommand command) {
        command.execute();
        undoStack.push(command);
    }

    public boolean isUndoAvailable() {
        return !undoStack.empty();
    }

    public boolean isRedoAvailable() {
        return !redoStack.empty();
    }

    public void undoCommand() {
        assert (!undoStack.empty());
        AddCommand command = undoStack.pop();
        command.undo();
        redoStack.push(command);
    }

    public void redoCommand() {
        assert (!redoStack.empty());
        AddCommand command = redoStack.pop();
        command.redo();
        undoStack.push(command);
    }
}
