package command;

import com.mygdx.game.Constant;
import com.mygdx.game.Game;
import entities.BasicPig;

public class AddBasicPigCommand implements AddCommand {

    private BasicPig.PigMemento pigMemento;
    private BasicPig pig;

    public AddBasicPigCommand(BasicPig pig){
        this.pig = pig;
    }
    @Override
    public void execute() {
        pigMemento = pig.createMemento();
        Game.addToGame(pig);
    }

    @Override
    public void undo() {
        Game.removeFromGame(pig);
        pig.removeBody();
        pig = null;
    }

    @Override
    public void redo() {
        pig = new BasicPig(Constant.getWorld(), pigMemento.getxPosition()
                , pigMemento.getyPosition(), pigMemento.getRot());
        pig.assemble();
        Game.addToGame(pig);
    }
}
