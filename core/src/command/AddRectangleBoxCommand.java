package command;

import com.mygdx.game.Constant;
import com.mygdx.game.Game;
import entities.RectangleBox;

public class AddRectangleBoxCommand implements AddCommand {

    private RectangleBox.RectangleBoxMemento rectangleBoxMemento;
    private RectangleBox rectangleBox;

    public AddRectangleBoxCommand(RectangleBox rectangleBox) {
        this.rectangleBox = rectangleBox;
    }

    @Override
    public void execute() {
        rectangleBoxMemento = rectangleBox.createMemento();
        Game.addToGame(rectangleBox);
    }

    @Override
    public void undo() {
        Game.removeFromGame(rectangleBox);
        rectangleBox.removeBody();
        rectangleBox = null;
    }

    @Override
    public void redo() {
        rectangleBox = new RectangleBox(Constant.getWorld(), rectangleBoxMemento.getXPosition()
                , rectangleBoxMemento.getYPosition(), rectangleBoxMemento.getRot());
        Game.addToGame(rectangleBox);

    }
}
