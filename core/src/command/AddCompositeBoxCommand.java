package command;

import com.mygdx.game.Constant;
import com.mygdx.game.Game;
import entities.Container;

public class AddCompositeBoxCommand implements AddCommand {

    private Container.ContainerMemento containerMemento;
    private Container container;


    public AddCompositeBoxCommand(Container container) {
        this.container = container;
    }

    @Override
    public void execute() {
        containerMemento = container.createMemento();
        Game.addToGame(container);
    }

    @Override
    public void undo() {
        Game.removeFromGame(container);
        container.removeContainer();
    }

    @Override
    public void redo() {
        container = new Container(Constant.getWorld(), containerMemento.getxPos()
                , containerMemento.getyPos(), containerMemento.getNumOfContainer(),
                containerMemento.getNumOfRectangle(), containerMemento.getNumOfSquare());
        Game.addToGame(container);
    }
}
