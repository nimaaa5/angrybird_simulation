package command;

import com.mygdx.game.Constant;
import com.mygdx.game.Game;
import entities.BasicPig;
import entities.PigWithHat;

public class AddPigWithHatCommand implements AddCommand{

    private PigWithHat pigWithHat;
    private BasicPig.PigMemento basicPigMemento;

    public AddPigWithHatCommand(PigWithHat pigWithHat){
        this.pigWithHat = pigWithHat;
    }

    @Override
    public void execute() {
        basicPigMemento = pigWithHat.getPig().createMemento();
        Game.addToGame(pigWithHat);
    }

    @Override
    public void undo() {
        Game.removeFromGame(pigWithHat);
        pigWithHat.getPig().removeBody();
        pigWithHat.removeBody();
        pigWithHat = null;
    }

    @Override
    public void redo() {
        pigWithHat = new PigWithHat(new BasicPig(Constant.getWorld(),
                basicPigMemento.getxPosition(),
                basicPigMemento.getyPosition(),
                basicPigMemento.getRot()));
        pigWithHat.assemble();
        Game.addToGame(pigWithHat);
    }
}
