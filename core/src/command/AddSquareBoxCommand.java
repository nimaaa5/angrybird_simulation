package command;

import com.mygdx.game.Constant;
import com.mygdx.game.Game;
import entities.SquareBox;

public class AddSquareBoxCommand implements AddCommand {

    private SquareBox.SquareBoxMemento squareBoxMemento;
    private SquareBox squareBox;


    public AddSquareBoxCommand(SquareBox squareBox) {
        this.squareBox = squareBox;
    }


    @Override
    public void execute() {
        squareBoxMemento = squareBox.createMemento();
        Game.addToGame(squareBox);
    }



    @Override
    public void undo() {
        Game.removeFromGame(squareBox);
        squareBox.removeBody();
        squareBox = null;
    }

    @Override
    public void redo() {
        squareBox = new SquareBox(Constant.getWorld(), squareBoxMemento.getXPosition()
                , squareBoxMemento.getYPosition(), squareBoxMemento.getRot());
        Game.addToGame(squareBox);
    }
}
