package command;

import com.mygdx.game.Constant;
import com.mygdx.game.Game;
import entities.BasicPig;
import entities.PigWithShield;

public class AddPigWithShieldCommand implements AddCommand {

    private PigWithShield pigWithShield;
    private BasicPig.PigMemento basicPigMemento;

    public AddPigWithShieldCommand(PigWithShield pig){
        this.pigWithShield = pig;
    }
    @Override
    public void execute() {
        basicPigMemento = pigWithShield.getPig().createMemento();
        Game.addToGame(pigWithShield);
    }

    @Override
    public void undo() {
        Game.removeFromGame(pigWithShield);
        pigWithShield.getPig().removeBody();
        pigWithShield.removeBody();
        pigWithShield = null;

    }

    @Override
    public void redo() {
        pigWithShield = new PigWithShield(new BasicPig(Constant.getWorld(),
                basicPigMemento.getxPosition(),
                basicPigMemento.getyPosition(),
                basicPigMemento.getRot()));
        pigWithShield.assemble();
        Game.addToGame(pigWithShield);
    }
}
