package Strategy;

import com.badlogic.gdx.math.Vector2;

public class ProjectileMotion extends MotionStrategy {


    public ProjectileMotion(Vector2 initialVelocity, Vector2 initialPoint) {
        super(initialVelocity, initialPoint);
    }

    @Override
    public float getX(float time) {
        return (float) initialVelocity.x * time + initialPoint.x;
    }

    @Override
    public float getY(float time) {
        return (float) (-.5 * 10 * time * time + initialVelocity.y * time + initialPoint.y);
    }
}
