package Strategy;

import com.badlogic.gdx.math.Vector2;

public class LinearMotion extends MotionStrategy {


    public LinearMotion(Vector2 initialVelocity, Vector2 initialPoint) {
        super(initialVelocity, initialPoint);
    }

    @Override
    public float getX(float time) {
        return 3 * time + initialPoint.x;
    }

    @Override
    public float getY(float time) {
        return -3 * time + initialPoint.y;
    }
}
