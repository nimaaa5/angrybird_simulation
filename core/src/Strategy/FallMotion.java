package Strategy;

import com.badlogic.gdx.math.Vector2;

public class FallMotion extends MotionStrategy{
    public FallMotion(Vector2 initialVelocity, Vector2 initialPoint) {
        super(initialVelocity, initialPoint);
    }

    @Override
    public float getX(float time) {
        return initialPoint.x;
    }

    @Override
    public float getY(float time) {

        return -8 * time + initialPoint.y;
    }
}
