package Strategy;

import com.badlogic.gdx.math.Vector2;

public abstract class MotionStrategy {

    protected Vector2 initialVelocity;
    protected Vector2 initialPoint;

    public MotionStrategy(Vector2 initialVelocity, Vector2 initialPoint) {
        this.initialVelocity = initialVelocity;
        this.initialPoint = initialPoint;
    }

    public abstract float getX(float time);

    public abstract float getY(float time);
}
