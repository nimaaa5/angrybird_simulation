package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ExtendViewport;


public class Constant {

    public static TextureAtlas TEXTURE_ATLAS;
    public final static float SCALE = 1.f / 16f;
    public final static float VP_WIDTH = Gdx.graphics.getWidth() * SCALE;
    public final static float VP_HEIGHT = Gdx.graphics.getHeight() * SCALE;


    static void initGame() {
        TEXTURE_ATLAS = new TextureAtlas("sprites.txt");
        world = new World(new Vector2(0, -10), true);
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new ExtendViewport(VP_WIDTH, VP_HEIGHT, camera);
    }


    static void StepWorld() {
        float delta = Gdx.graphics.getDeltaTime();
        accumulator += Math.min(delta, 0.25f);

        if (accumulator >= STEP_TIME) {
            accumulator -= STEP_TIME;
            world.step(STEP_TIME, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
        }
    }


    static void setResizableScreen(int width, int height) {
        viewport.update(width, height, true);
        batch.setProjectionMatrix(camera.combined);
    }

    public static World getWorld() {
        return world;
    }

    public static SpriteBatch getBatch() {
        return batch;
    }

    public static OrthographicCamera getCamera() {
        return camera;
    }


    private static SpriteBatch batch;
    private static OrthographicCamera camera;
    private static ExtendViewport viewport;
    private static World world;

    private static final float STEP_TIME = 1f / 60f;
    private static final int VELOCITY_ITERATIONS = 6;
    private static final int POSITION_ITERATIONS = 2;
    private static float accumulator = 0;

}
