package com.mygdx.game;

import command.AddCommandManager;
import command.AddCompositeBoxCommand;
import command.AddRectangleBoxCommand;
import command.AddSquareBoxCommand;
import entities.*;
import entities.Container;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class AddBoxWindow extends JFrame {

    private AddCommandManager commandManager;
    private JRadioButton squareBoxBtn;
    private JRadioButton rectangleBoxBtn;
    private JRadioButton compositeBoxBtn;
    private JPanel panel = new JPanel();
    private JPanel boxSpecificationPanel = new JPanel();
    private JLabel positionLabel = new JLabel("Position : ");
    private JTextField positionTextField = new JTextField(5);
    private JLabel squareNumberLabel = new JLabel("Number of Square : ");
    private JTextField squareNumberTextField = new JTextField(5);
    private JLabel rectNumberLabel = new JLabel("Number of Rectangle : ");
    private JTextField rectNumberTextField = new JTextField(5);
    private JLabel containerNumberLabel = new JLabel("Number of Container : ");
    private JTextField containerNumberTextField = new JTextField(5);
    private JButton createButton;

    AddBoxWindow(String message, AddCommandManager manager) {
        super(message);
        setSize(600, 200);
        setLocationRelativeTo(null);
        add(panel);
        placeComponents(panel);
        setVisible(true);
        this.commandManager = manager;
    }


    private void placeComponents(JPanel panel) {

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel chooseBoxLbl = new JLabel("Please choose type of the box");
        JPanel radioPanel = new JPanel();
        placeRadioButtonComponent(radioPanel);
        panel.add(chooseBoxLbl);
        panel.add(radioPanel);
        panel.add(boxSpecificationPanel);
        createButton = new JButton("Create");
        createButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (squareBoxBtn.isSelected()) {
                    commandManager.executeCommand(new AddSquareBoxCommand(new SquareBox(Constant.getWorld(),
                            Integer.parseInt(positionTextField.getText()), 10, 0)));
                } else if (rectangleBoxBtn.isSelected()) {
                    commandManager.executeCommand(new AddRectangleBoxCommand(new RectangleBox(Constant.getWorld(),
                            Integer.parseInt(positionTextField.getText()), 10, 0)));
                } else if (compositeBoxBtn.isSelected()) {
                    commandManager.executeCommand(new AddCompositeBoxCommand(new Container(Constant.getWorld(), Integer.parseInt(positionTextField.getText()),
                            10, Integer.parseInt(containerNumberTextField.getText()),
                            Integer.parseInt(rectNumberTextField.getText()),
                            Integer.parseInt(squareNumberTextField.getText()))));
                }
            }
        });
        panel.add(createButton);


    }

    private void placeRadioButtonComponent(JPanel radioPanel) {
        squareBoxBtn = new JRadioButton("Square Box");
        rectangleBoxBtn = new JRadioButton("Rectangle Box");
        compositeBoxBtn = new JRadioButton("Composite Box");
        ButtonGroup group = new ButtonGroup();
        group.add(squareBoxBtn);
        group.add(rectangleBoxBtn);
        group.add(compositeBoxBtn);
        radioPanel.add(squareBoxBtn);
        radioPanel.add(rectangleBoxBtn);
        radioPanel.add(compositeBoxBtn);

        boxSpecificationPanel.setLayout(new GridLayout(2, 4));
        squareBoxBtn.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                placeSquareBoxPanel(boxSpecificationPanel);
                panel.updateUI();
            }
        });

        rectangleBoxBtn.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                placeSquareBoxPanel(boxSpecificationPanel);
                panel.updateUI();
            }
        });

        compositeBoxBtn.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                placeCompositePanel(boxSpecificationPanel);
                panel.updateUI();
            }
        });

    }


    private void placeSquareBoxPanel(JPanel panel) {
        positionLabel.setVisible(true);
        positionTextField.setVisible(true);
        containerNumberLabel.setVisible(false);
        containerNumberTextField.setVisible(false);
        rectNumberLabel.setVisible(false);
        rectNumberTextField.setVisible(false);
        squareNumberLabel.setVisible(false);
        squareNumberTextField.setVisible(false);
        panel.add(positionLabel);
        panel.add(positionTextField);
        panel.add(containerNumberLabel);
        panel.add(containerNumberTextField);
        panel.add(squareNumberLabel);
        panel.add(squareNumberTextField);
        panel.add(rectNumberLabel);
        panel.add(rectNumberTextField);
    }

    private void placeCompositePanel(JPanel panel) {
        positionLabel.setVisible(true);
        positionTextField.setVisible(true);
        containerNumberLabel.setVisible(true);
        containerNumberTextField.setVisible(true);
        rectNumberLabel.setVisible(true);
        rectNumberTextField.setVisible(true);
        squareNumberLabel.setVisible(true);
        squareNumberTextField.setVisible(true);
        panel.add(positionLabel);
        panel.add(positionTextField);
        panel.add(containerNumberLabel);
        panel.add(containerNumberTextField);
        panel.add(squareNumberLabel);
        panel.add(squareNumberTextField);
        panel.add(rectNumberLabel);
        panel.add(rectNumberTextField);
        panel.updateUI();
    }


}


