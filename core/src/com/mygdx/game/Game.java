package com.mygdx.game;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import entities.BaseEntity;
import entities.Bird;
import theme.GUIFactory;
import theme.MorningThemeFactory;
import theme.ThemeFactory;

import java.util.ArrayList;

public class Game extends ApplicationAdapter implements InputProcessor {

    private GUIFactory guiFactory;
    private Box2DDebugRenderer renderer;
    private static ArrayList<BaseEntity> entities;
    private static Bird bird;
    private Toolbar toolbar;


    @Override
    public void create() {
        Constant.initGame();
        entities = new ArrayList<BaseEntity>();
        guiFactory = new GUIFactory();
        ThemeFactory themeFactory = MorningThemeFactory.getInstance();
        guiFactory.buildGUI(themeFactory);
        renderer = new Box2DDebugRenderer();
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        toolbar = new Toolbar(inputMultiplexer);
        toolbar.setTheme(guiFactory);
        inputMultiplexer.addProcessor(this);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    public static void addToGame(BaseEntity entity) {
        entities.add(entity);
    }

    public static void removeFromGame(BaseEntity entity) {
        entities.remove(entity);
    }

    public static void addBirdToGame(Bird bird) {
        Game.bird = bird;
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Constant.StepWorld();
        Constant.getBatch().setProjectionMatrix(Constant.getCamera().combined);
        guiFactory.drawGUI(Constant.getBatch());
        guiFactory.drawShot();
        if (bird != null)
            bird.draw();
        guiFactory.drawSlingShotPart(Constant.getBatch());
        toolbar.drawStage();
        for (BaseEntity entity : entities) {
            entity.draw(Constant.getBatch());
        }

        renderer.render(Constant.getWorld(), Constant.getCamera().combined);
    }

    @Override
    public void resize(int width, int height) {
        Constant.setResizableScreen(width, height);
        toolbar.resize(width, height);
        guiFactory.getGround().createBody();
    }

    @Override
    public void dispose() {
        Constant.getBatch().dispose();
        toolbar.stage.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    private boolean dragging = false;
    private BaseEntity selectedEntity;

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (bird != null) {
            if ((screenX * Constant.SCALE < 10 + bird.getBirdImage().getWidth()
                    && 10 < screenX * Constant.SCALE)
                    && ((Gdx.graphics.getHeight() - screenY) * Constant.SCALE < 10 + bird.getBirdImage().getHeight()
                    && 10 < (Gdx.graphics.getHeight() - screenY) * Constant.SCALE)) {
                bird.setCharging(true);
                guiFactory.getSlingshot().setCharging(true);
                isCharging = true;
            }
        }
        if (Input.Buttons.RIGHT == button) {
            for (BaseEntity entity : entities) {
                if (entity.isInside((screenX * Constant.SCALE),
                        ((Gdx.graphics.getHeight() - screenY) * Constant.SCALE))) {
                    selectedEntity = entity;
                    dragging = true;
                    break;
                }
            }
        }
        return true;

    }

    private boolean isCharging = false;

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        dragging = false;
        selectedEntity = null;

        if (bird != null) {
            bird.setPosition();
            bird.setInitialVelocity();
            bird.setReleased(true);
            bird.setReleased(true);
            guiFactory.getSlingshot().setReleased(true);
            guiFactory.getSlingshot().setCharging(false);
        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (dragging) {
            selectedEntity.getBody().setTransform(screenX * Constant.SCALE,
                    (Gdx.graphics.getHeight() - screenY) * Constant.SCALE, 0);
        }
        if (isCharging) {
            bird.getBody().setTransform(screenX * Constant.SCALE,
                    (Gdx.graphics.getHeight() - screenY) * Constant.SCALE, 0);
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
