package com.mygdx.game;

import Strategy.ProjectileMotion;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import command.AddCommandManager;
import entities.Bird;
import theme.AfternoonThemeFactory;
import theme.GUIFactory;
import theme.MorningThemeFactory;


public class Toolbar {
    Stage stage;
    private FitViewport viewport;
    private OrthographicCamera camera;

    private Image addBirdButton;
    private Image addPigButton;
    private Image addBoxButton;
    private Image undoButton;
    private Image redoButton;
    private Image afternoonButton;
    private Image morningButton;

    private AddCommandManager manager;

    Toolbar(InputMultiplexer inputMultiplexer) {
        manager = new AddCommandManager();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Constant.VP_WIDTH, Constant.VP_HEIGHT, camera);
        stage = new Stage(viewport);
        Table table = initImages();
        stage.addActor(table);
        inputMultiplexer.addProcessor(stage);

    }


    private Table initImages() {
        Table table = new Table();
        table.left().top();
        table.pad(1);
        table.setPosition(0, Constant.VP_HEIGHT);
        addBirdButton = new Image(new Texture("add_bird.png"));
        addPigButton = new Image(new Texture("add_pig.png"));
        addBoxButton = new Image(new Texture("add_box.png"));
        undoButton = new Image(new Texture("undo.png"));
        redoButton = new Image(new Texture("redo.png"));
        afternoonButton = new Image(new Texture("morning.png"));
        morningButton = new Image(new Texture("afternoon.png"));

        addBirdButton.setSize(addBirdButton.getWidth() * Constant.SCALE,
                addBirdButton.getHeight() * Constant.SCALE);

        addBirdButton.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Bird bird = new Bird();
                Game.addBirdToGame(bird);
                return true;
            }
        });

        addPigButton.setSize(addPigButton.getWidth() * Constant.SCALE,
                addPigButton.getHeight() * Constant.SCALE);

        addPigButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                new AddPigFrame("Creating Pig", manager);
                return true;
            }
        });

        addBoxButton.setSize(addBoxButton.getWidth() * Constant.SCALE,
                addBoxButton.getHeight() * Constant.SCALE);
        addBoxButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                new AddBoxWindow("Add Box", manager);
                return true;
            }
        });

        undoButton.setSize(undoButton.getWidth() * Constant.SCALE,
                undoButton.getHeight() * Constant.SCALE);

        undoButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (manager.isUndoAvailable())
                    manager.undoCommand();

                return true;
            }
        });


        redoButton.setSize(redoButton.getWidth() * Constant.SCALE,
                redoButton.getHeight() * Constant.SCALE);

        redoButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (manager.isRedoAvailable())
                    manager.redoCommand();
                return true;
            }
        });


        morningButton.setSize(morningButton.getWidth() * Constant.SCALE,
                morningButton.getHeight() * Constant.SCALE);


        afternoonButton.setSize(afternoonButton.getWidth() * Constant.SCALE,
                afternoonButton.getHeight() * Constant.SCALE);

        table.add(addBirdButton).size(addBirdButton.getWidth(), addBirdButton.getHeight());
        table.add(addPigButton).size(addPigButton.getWidth(), addPigButton.getHeight());
        table.add(addBoxButton).size(addBoxButton.getWidth(), addBoxButton.getHeight());
        table.add(undoButton).size(undoButton.getWidth(), undoButton.getHeight());
        table.add(redoButton).size(redoButton.getWidth(), redoButton.getHeight());
        table.add(morningButton).size(morningButton.getWidth(), morningButton.getHeight());
        table.add(afternoonButton).size(afternoonButton.getWidth(), afternoonButton.getHeight());
        return table;
    }

    void drawStage() {
        stage.act();
        stage.draw();
    }

    void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        stage.getBatch().setProjectionMatrix(camera.combined);

    }


    public void setTheme(final GUIFactory factory) {

        morningButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AfternoonThemeFactory.setInstance();
                factory.buildGUI(AfternoonThemeFactory.getInstance());
                return true;
            }
        });

        afternoonButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                MorningThemeFactory.setInstance();
                factory.buildGUI(MorningThemeFactory.getInstance());
                return true;
            }
        });
    }


}
