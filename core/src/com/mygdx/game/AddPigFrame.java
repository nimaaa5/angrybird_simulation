package com.mygdx.game;

import command.AddBasicPigCommand;
import command.AddCommandManager;
import command.AddPigWithHatCommand;
import command.AddPigWithShieldCommand;
import entities.BasicPig;
import entities.PigWithHat;
import entities.PigWithShield;

import javax.swing.*;
import java.awt.event.ActionEvent;

 class AddPigFrame extends JFrame {

    private JPanel panel;
    private JTextField positionTextfield;
    private JRadioButton basicPigBtn;
    private JRadioButton hatPigBtn;
    private JRadioButton shieldPigBtn;
    private AddCommandManager commandManager;

     AddPigFrame(String message, AddCommandManager manager) {
        super(message);
        setSize(600, 200);
        setLocationRelativeTo(null);
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        add(panel);
        placeComponent();
        setVisible(true);
        this.commandManager = manager;
    }

    private void placeComponent() {

        JPanel radioPanel = new JPanel();

        basicPigBtn = new JRadioButton("Basic Pig", new ImageIcon("healthy_pig.png"));
        hatPigBtn = new JRadioButton("Pig with hat", new ImageIcon("hat.png"));
        shieldPigBtn = new JRadioButton("Pig with shield", new ImageIcon("shield.png"));

        ButtonGroup btnGroup = new ButtonGroup();
        btnGroup.add(basicPigBtn);
        btnGroup.add(hatPigBtn);
        btnGroup.add(shieldPigBtn);

        radioPanel.add(basicPigBtn);
        radioPanel.add(hatPigBtn);
        radioPanel.add(shieldPigBtn);


        JPanel positionPanel = new JPanel();
        JLabel positionLbl = new JLabel("Position :");
        positionTextfield = new JTextField(5);
        positionPanel.add(positionLbl);
        positionPanel.add(positionTextfield);
        JButton createBtn = new JButton("Create");

        createBtn.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BasicPig basicPig = new BasicPig(Constant.getWorld(),
                        Integer.parseInt(positionTextfield.getText()),
                        10, 0);
                if (basicPigBtn.isSelected()) {
                    basicPig.assemble();
                    commandManager.executeCommand(new AddBasicPigCommand(basicPig));
                } else if (hatPigBtn.isSelected()) {
                    PigWithHat pigWithHat = new PigWithHat(basicPig);
                    pigWithHat.assemble();
                    commandManager.executeCommand(new AddPigWithHatCommand(pigWithHat));
                } else if (shieldPigBtn.isSelected()) {
                    PigWithShield pigWithShield = new PigWithShield(basicPig);
                    pigWithShield.assemble();
                   commandManager.executeCommand(new AddPigWithShieldCommand(pigWithShield));
                }
            }
        });
        panel.add(radioPanel);
        panel.add(positionPanel);
        panel.add(createBtn);
    }

}
